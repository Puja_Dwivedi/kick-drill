//
//  KickDrillApp.swift
//  KickDrill
//
//  Created by cis on 11/01/23.
//

import SwiftUI

@main
struct KickDrillApp: App {
    var body: some Scene {
        WindowGroup {
            LaunchScreenView()
        }
    }
}
