//
//  LoginView.swift
//  KickDrill
//
//  Created by cis on 11/01/23.
//

import SwiftUI

struct LoginView: View {
    
    @State private var moveToHomeView: Bool = false
    @State private var rememberMeTapped: Bool = false
    @State private var email: String = ""
    @State private var password: String = ""
    
    var body: some View {
        VStack {
            Image("")
                .resizable()
                .frame(height: 44)
                .background(Colors.AppColor)
                .edgesIgnoringSafeArea(.all)
            Spacer()
            VStack {
                Image("logo")
                    .frame(width: 173, height: 142, alignment: .center)
                Spacer()
                VStack(spacing: 0) {
                    VStack(spacing: 5) {
                        TextField("Email", text: $email)
                            .frame(width: 320, height: 30, alignment: .center)
                            .foregroundColor(Colors.BlackColor)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 25)
                                .fill(Color.white))
                            .compositingGroup()     // << here !!
                            .shadow(color: Colors.ViewShadowGrayColor.opacity(0.2), radius: 5) // << shadow to all composition
                            .padding()
                        
                        TextField("Password", text: $password)
                            .frame(width: 320, height: 30, alignment: .center)
                            .foregroundColor(Colors.BlackColor)
                            .padding()
                            .background(RoundedRectangle(cornerRadius: 25)
                                .fill(Color.white))
                            .compositingGroup()     // << here !!
                            .shadow(color: Colors.ViewShadowGrayColor.opacity(0.2), radius: 5) // << shadow to all composition
                            .padding()
                        
                    }
                    
                    HStack {
                        Image("box")
                        Button("remember_me".localizedString()) {
                            print("Remember me tapped")
                        }.foregroundColor(Colors.TxtGrayColor)
                            .font(Fonts.SFProTextRegular12)
                        Spacer()
                        Button {
                            print("Forgot password tapped")
                        } label: {
                            Text("forgot_password".localizedString()).underline()
                        }.foregroundColor(Colors.AppColor)
                            .font(Fonts.SFProTextRegular15)
                    }.padding(.leading, 25)
                        .padding(.trailing)
                    Spacer()
                }.padding(.top, 66)
                Spacer()
                VStack(spacing: 20) {
                    Button("login".localizedString()) {
                        print("Login tapped")
                    }.frame(width: 300, height: 50, alignment: .center)
                        .background(Colors.AppColor)
                        .foregroundColor(Colors.WhiteColor)
                        .font(Fonts.PoppinsMedium18)
                        .cornerRadius(25)
                    
                    HStack {
                        Text("dont_have_account".localizedString())
                            .foregroundColor(Colors.TxtGrayColor)
                            .font(Fonts.SFProTextRegular16)
                        Button("sign_up".localizedString()) {
                            print("Signup tapped")
                        }
                        .foregroundColor(Colors.AppColor)
                        .font(Fonts.SFProTextSemibold16)
                    }
                    
                    
                }
                
                
                
            }
            
        }.navigationBarHidden(true)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView().previewDevice("iPhone 13")
    }
}
