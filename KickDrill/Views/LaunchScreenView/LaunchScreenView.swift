//
//  ContentView.swift
//  KickDrill
//
//  Created by cis on 11/01/23.
//

import SwiftUI

struct LaunchScreenView: View {
    
    @State private var isGetStartedHidden: Bool = true
    @State private var moveToLoginView: Bool = false
    
    var body: some View {
        NavigationView {
            ZStack {
                Image("splash_background")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
                Colors.SplashBackgrndColor.ignoresSafeArea(.all)
                VStack {
                    VStack(spacing: 30) {
                        Spacer()
                        Image("logo")
                        Text("slogan".localizedString())
                            .foregroundColor(Colors.AppColor)
                            .font(Fonts.PoppinsBold20)
                        Spacer()
                        NavigationLink(destination: LoginView(), isActive: $moveToLoginView) {
                            Button("Get Started") {
                                self.moveToLoginView = true
                            }.frame(width: 240, height: 50, alignment: .center)
                                .background(Colors.AppColor)
                                .foregroundColor(Colors.WhiteColor)
                                .font(Fonts.PoppinsMedium20)
                                .cornerRadius(25)
                                .opacity(isGetStartedHidden == true ? 0.0 : 1.0)
                        }
                    }
                }.padding()
            }.onAppear(){
                showGetStarted()
            }
        }
        
    }
    
    private func showGetStarted() {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                    withAnimation {
                        self.isGetStartedHidden = false
                    }
                }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchScreenView().previewDevice("iPhone 13")
    }
}

