//
//  GlobalMethods.swift
//  MCQ
//
//  Created by cis on 28/01/22.
//

import UIKit

var isUserLogin:Bool
{
    return (UserDefaults.standard.value(forKey: UserDefaultsKey.kIsLogin) != nil) ? UserDefaults.standard.value(forKey: UserDefaultsKey.kIsLogin) as! Bool : false
}


func removeUserDefaultDataLogout()
{
    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.kIsLogin)
    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.kAuthToken)
    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.kProfileName)
    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.kProfileImage)
    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.kUserId)
}

enum Environment {
    case dev
    case live
    case none
}
