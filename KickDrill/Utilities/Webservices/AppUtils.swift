//
//  AppUtils.swift
//  RideShare
//
//  Created by cis on 12/09/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
struct AppUtils {
    static func removeAllUserDefaults(){
        UserDefaults.standard.removeObject(forKey: UserDefaults.keys.User.user_model)
        UserDefaults.standard.synchronize()
    }
//    static func saveUserDefault(userModel : UserDetailData){
//        let encoder = JSONEncoder()
//        if let encoded = try? encoder.encode(userModel){
//            UserDefaults.standard.set(encoded, forKey: UserDefaults.keys.User.user_model)
//            UserDefaults.standard.synchronize()
//        }
//    }
//    
// 
//    static func getUserDefault() -> UserDetailData?{
//        if let data = UserDefaults.standard.object(forKey: UserDefaults.keys.User.user_model) as? Data{
//            let decoder = JSONDecoder()
//            if let driverModel = try? decoder.decode(UserDetailData.self, from: data){
//                return driverModel
//            }
//        }
//        return nil
//    }
   
    static func convertStringToDate(from : String) -> NSDate{
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone(identifier:"UTC")
        dateFormate.dateFormat = "yyyy-MM-ddHH:mm:ss"
        let date = dateFormate.date(from: from)
        return date! as NSDate
    }
    static func convertDateToString(from : NSDate) -> String{
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone(identifier:"UTC")
        dateFormate.dateFormat = "yyyy-MM-ddHH:mm:ss"
        let date = dateFormate.string(from: from as Date)
        return date
    }
    static func getTimeDifferencefromDate(strDate : String) -> String{
        
        let currentDate = self.convertStringToDate(from: strDate) as Date
        let now = Date()
        let strDifference = now.offset(from: currentDate)
        return strDifference
    }
    static func getTimeDifferenceInSeconds(strDate : String) -> String{
        let currentDate = self.convertStringToDate(from: strDate) as Date
        let now = Date()
        let strDifference = now.offsetSeconds(from: currentDate)
        return strDifference
    }
    
    static func getDateFromString(strDate : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: strDate)
        if date != nil {
            dateFormat.dateFormat = "dd/MM/yyyy"
            return dateFormat.string(from: date!)
        }else{
            return ""
        }
    }
    
    static func getTimeFromString(strTime : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let time = dateFormat.date(from: strTime)
        if time != nil {
            dateFormat.dateFormat = "HH:mm"
            return dateFormat.string(from: time!)
        }else{
            return ""
        }
    }
}
