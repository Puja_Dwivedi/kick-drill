//
//  WebUrl.swift
//  FastAide
//
//  Created by cis on 21/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct WebUrl {
    static let baseUrl                       = "https://api.kickdrill.com/api/" //"https://kr.cisinlive.com/rswellcontrol/api/"
    
    static let formulasUrl                   = "https://kickdrill.com/formulas/" //"https://rt.cisinlive.com/mcq-hm/formulas/"
    
    static let killsheetsUrl                 = "https://kickdrill.com/killsheet/"
    
    static let sandboxUrlVerifyReceipt        = "https://sandbox.itunes.apple.com/verifyReceipt"
    
    static let liveUrlVerifyReceipt           = "https://buy.itunes.apple.com/verifyReceipt"
    
    static let termsUrl                      = ""
    
    static let imagePathUrl                  = ""
    
    static let accessToken                   = baseUrl + "getAPIAccessToken"
        
    static let userLogin                     = baseUrl + "login"
    
    static let userRegistration              = baseUrl + "userRegistration"
    
    static let sendPasswordResetLink         = baseUrl + "sendPasswordResetLink"
    
    static let changePassword                = baseUrl + "changePassword"
    
    static let logout                        = baseUrl + "logout"
    
    static let requestMobileOTP              = baseUrl + "requestMobileOTP"
    
    static let verifyMobileOTP               = baseUrl + "verifyMobileOTP"
    
    static let requestOTP                   = baseUrl + "requestOTP"
    
    static let verifyOTP                    = baseUrl + "verifyOTP"
    
    static let addPurchaseDetail            = baseUrl + "addPurchaseDetail"
    
    static let getPurchaseDetail            = baseUrl + "getPurchaseDetail"
    
    static let getAppContent                 = baseUrl + "getAppContent"
    
    static let getUserDetails                = baseUrl + "geUserDetails"
    
    static let updateUserProfile             = baseUrl + "updateUserProfile"
    
    static let getPlanList                   = baseUrl + "getPlanList"
    
    static let getCategories                 = baseUrl + "getCategories"
    
    static let getSubCategories              = baseUrl + "getSubCategories"
    
    static let getPracticeQuestions          = baseUrl + "getPracticeQuestions"
    
    static let getExamQuestions              = baseUrl + "getExamQuestions"
    
    static let postExam                      = baseUrl + "postExam"
    
    static let getBasicInfo                  = baseUrl + "getBasicInfo"
    
    static let getUpdatedRecords             = baseUrl + "getUpdatedRecords"
    
    static let myAccount                     = baseUrl + "myAccount"
    
    static let getUserAnalitics              = baseUrl + "getUserAnalitics"
    
    static let clearUserAnalytics            = baseUrl + "clearUserAnalytics"
    
    static let getSubscriptionStatus          = baseUrl + "getSubscriptionStatus"
    
    static let getCategories_dev             = baseUrl + "getCategories_dev"
    
    static let getSubCategories_dev          = baseUrl + "getSubCategories_dev"
    
    static let getPracticeQuestions_dev      = baseUrl + "getPracticeQuestions_dev"
    
    static let getExamQuestions_dev          = baseUrl + "getExamQuestions_dev"
    
    static let postExam_dev                  = baseUrl + "postExam_dev"
    
    static let getUpdatedRecords_dev         = baseUrl + "getUpdatedRecords_dev"
    
    static let getUserAnalitics_dev          = baseUrl + "getUserAnalitics_dev"
    
    static let clearUserAnalytics_dev        = baseUrl + "clearUserAnalytics_dev"
    
    static let getExamReport                = baseUrl + "getExamReport"
    
    static let deleteMyAccount              = baseUrl + "deleteMyAccount"
}


//struct Header {
//
//    static let accessTokenHeader = ["Accept": "application/json"]
//
//    static let header = ["Accept": "application/json", "API-ACCESS-TOKEN": UserDefaults.standard.value(forKey: UserDefaultsKey.kAccessToken) as? String ?? ""]
//
//    static let authTokenHeader = ["Accept": "application/json", "API-ACCESS-TOKEN": UserDefaults.standard.value(forKey: UserDefaultsKey.kAccessToken) as? String ?? "", "Authorization" : "Bearer \(UserDefaults.standard.value(forKey: UserDefaultsKey.kAuthToken) as? String ?? "")"]
//
//}

struct Token {
    static let apsnsToken = "APf-g1w8-Q5Yrwjz8Z4"
}
