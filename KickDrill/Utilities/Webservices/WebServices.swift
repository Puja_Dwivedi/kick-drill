//
//  K3Webservices.swift
//  CityWall
//
//  Created by Karan Kumar Kanathe on 03/04/19.
//  Copyright © 2019 Karan Kanathe. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
enum K3encoding {
    case JSON
    case URL
}
class WebServices : NSObject {
    class func run<T:Decodable>(isMultipart:Bool = false,urlString:String,method : HTTPMethod,Param:[String:Any]?,header:[String:String]?,file:[String:Any]? = nil,type:T.Type,viewController : UIViewController?,fullAccess : Bool = false,encoding:K3encoding = K3encoding.JSON,Completion:@escaping (T?) -> ()) {
        print("Hitting Api => \(urlString)")
        print("With params => \(String(describing: Param!))")
        if Connectivity.isConnectedToInternet() == false{
            Loader.sharedInstance.stopLoader()
            AlertView.instance.show(Constants.APP_NAME, message: Constants.NO_INTERNET, handler: nil)
            return
        }
        
        if isMultipart{
            if viewController != nil{
                //
                //  ManagerSharedInstance.showIndicator()
                //  Loader.instance.show(view: (viewController?.view)!)
            }
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if file != nil{
                    let keys = file?.keys
                    for key in keys! {
                        if (file!["\(key)"] as? UIImage) != nil {
                            if let imageData = (file!["\(key)"] as! UIImage).jpegData(compressionQuality: 0.8) {
                                print("\(key)--\(imageData)")
                                multipartFormData.append(imageData, withName: "\(key)",fileName: "file.jpg", mimeType: "image/jpeg")
                            }
                        }
                        
                        if let imagess  = file!["\(key)"] as? Data {
                            print("\(key)--\(imagess)")
                            multipartFormData.append(imagess, withName: "\(key)",fileName: "file.mov", mimeType: "video/*")
                        }
                        
                        for key in keys! {
                            if let imagess  = file!["\(key)"] as? [UIImage] {
                                for img in imagess {
                                    if let imageData = img.jpegData(compressionQuality: 0.8) {
                                        print("\(key)--\(imageData)")
                                        multipartFormData.append(imageData, withName: "\(key)",fileName: "file.jpg", mimeType: "image/jpeg")
                                    }
                                }
                            }
                        }
                    }
                }
                if Param != nil{
                    
                    for (key, value) in Param! {
                        if let isString = value as? String {
                            print("\(key)--\(value)")
                            
                            multipartFormData.append(isString.data(using: String.Encoding.utf8)!, withName: key)
                        }
                        if let value = value as? Int {
                            print("\(key)--\(value)")
                            
                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                        if let value = value as? [[String:Int]] {
                            print("\(key)--\(value)")
                            
                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                        if let value = value as? [String:Any] {
                            print("\(key)--\(value)")
                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }
                    }
                }
                
            }, to: urlString, method: method, headers : header, encodingCompletion: { (results) in
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    switch results {
                    case .success(let upload,_,_):
                        upload.responseData(completionHandler: { (data) in
                            guard let isData = data.value else {return}
                            let strData = NSString(data: isData, encoding: String.Encoding.utf8.rawValue)
                            print("\(strData ?? "")")
                        })
                        upload.responseJSON(completionHandler: { (response) in
                            print(response)
                            switch response.result{
                            case .success:
                                if let isValue = response.value as? [String:Any]{
                                    if fullAccess{
                                        let jsonDecoder = JSONDecoder()
                                        do{
                                            let data = try jsonDecoder.decode(T.self, from: response.data!)
                                            Completion(data)
                                        }catch let error{
                                            print(error)
                                            
                                            Loader.sharedInstance.stopLoader()
                                            AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                                        }
                                    }else{
                                            if isValue["status"] as? Bool == true{
                                            let jsonDecoder = JSONDecoder()
                                            do{
                                                let data = try jsonDecoder.decode(T.self, from: response.data!)
                                                Completion(data)
                                            }catch let error{
                                                print(error)
                                                
                                                Loader.sharedInstance.stopLoader()
                                                AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                                            }
                                        }else{
                                            var message = ""
                                            
                                            if let isMessage = isValue["msg"] as? String{
                                                message = isMessage
                                            }
                                            
                                            if let isArrMessage = isValue["msg"] as? [String]{
                                                isArrMessage.forEach({
                                                    message.append("\($0)\n")
                                                })
                                            }
                                            let jsonDecoder = JSONDecoder()
                                            do{
                                                let data = try jsonDecoder.decode(T.self, from: response.data!)
                                                Completion(data)
                                            }catch let error{
                                                print(error)
                                                
                                                Loader.sharedInstance.stopLoader()
                                                if (isValue["msgCode"] as? Int) == 407 {
                                                    AlertView.instance.show(Constants.SESSION_EXPIRED, message: message, handler: {_ in
                                                        removeUserDefaultDataLogout()
                                                      //  AppDelegate.shared.moveToLoginVC()
                                                    })
                                                } else {
                                                    AlertView.instance.show(Constants.APP_NAME, message: message, handler: nil)
                                                }
                                                
                                            }
                                        }
                                    }
                                    
                                }else{
                                    
                                }
                                if viewController != nil{
                                    //                                    ManagerSharedInstance.hideIndicator()
                                    //                                    Loader.instance.hide()
                                }
                                break
                            case .failure(let error):
                                print(error)
                                Loader.sharedInstance.stopLoader()
                                
                                if viewController != nil{
                                    //                                    ManagerSharedInstance.hideIndicator()
                                    //                                    Loader.instance.hide()
                                }
                                AlertView.instance.show(Constants.APP_NAME, message: error.localizedDescription, handler: nil)
                                
                            }
                        })
                    case .failure(let error):
                        print(error)
                        // Show alert here
                        
                        Loader.sharedInstance.stopLoader()
                        if error.localizedDescription == "The network connection was lost."{
                            AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                            return
                        }
                        AlertView.instance.show(Constants.APP_NAME, message:  error.localizedDescription, handler: nil)
                    }
                })
            })
        }else{
            let url = URL(string: urlString)
            if viewController != nil{
                //                ManagerSharedInstance.showIndicator()
                //                Loader.instance.show(view: (viewController?.view!)!)
            }
            Alamofire.request(url!, method: method, parameters: Param, encoding: encoding == K3encoding.URL ? URLEncoding.default : JSONEncoding.default, headers : header).responseJSON { (resonse) in
                if viewController != nil{
                    //                        ManagerSharedInstance.hideIndicator()
                    //                        Loader.instance.hide()
                }
                
                switch resonse.result{
                case .success:
                    //                print("Response Value =>>> \(NSString(data: resonse.value as? Data ?? Data(), encoding: String.Encoding.utf8.rawValue) ?? "")")
                    print(resonse)
                    
                    if let isValue = resonse.value as? [String:Any]{
                        
                        if fullAccess{
                            let jsonDecoder = JSONDecoder()
                            do{
                                let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                Completion(data)
                                
                            }catch let error{
                                print(error)
                                
                                Loader.sharedInstance.stopLoader()
                                AlertView.instance.show(Constants.APP_NAME, message:  Constants.SOMETHING_WENT_WRONG, handler: nil)
                                
                            }
                        }else{
                            if isValue["status"] as? Bool == true{
                                
                                let jsonDecoder = JSONDecoder()
                                do{
                                    let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                    Completion(data)
                                    
                                }catch let error{
                                    print(error)
                                    
                                    Loader.sharedInstance.stopLoader()
                                    AlertView.instance.show(Constants.APP_NAME, message:  Constants.SOMETHING_WENT_WRONG, handler: nil)
                                    
                                }
                            }else{
                                var message = ""
                                
                                if let isMessage = isValue["msg"] as? String{
                                    message = isMessage
                                }
                                
                                if let isArrMessage = isValue["msg"] as? [String]{
                                    isArrMessage.forEach({
                                        message.append("\($0)\n")
                                    })
                                }
                                let jsonDecoder = JSONDecoder()
                                do{
                                    let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                    Completion(data)
                                }catch let error{
                                    print(error)
                                        
                                    Loader.sharedInstance.stopLoader()
                                    AlertView.instance.show(Constants.APP_NAME, message: message, handler: nil)
                                }
                                
                            }
                        }
                    }else{
                        Loader.sharedInstance.stopLoader()
                        AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                        
                    }
                    break
                case .failure(let error):
                    print(error)
                    // Show alert here
                    
                    Loader.sharedInstance.stopLoader()
                    if error.localizedDescription == "The network connection was lost."{
                     //   AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                        return
                    }
                    AlertView.instance.show(Constants.APP_NAME, message:  error.localizedDescription, handler: nil)
                    
                }
            }
        }
    }
}


