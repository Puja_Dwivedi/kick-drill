//
//  Constants.swift
//  RideShare
//
//  Created by cis on 17/08/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct Fonts {
    static let SFProTextRegular12 = Font.custom(FontName.SFProText.regular.description, size: 12)
    static let SFProTextRegular13 = Font.custom(FontName.SFProText.regular.description, size: 13)
    static let SFProTextRegular14 = Font.custom(FontName.SFProText.regular.description, size: 14)
    static let SFProTextRegular15 = Font.custom(FontName.SFProText.regular.description, size: 15)
    static let SFProTextRegular16 = Font.custom(FontName.SFProText.regular.description, size: 16)
    static let SFProTextRegular17 = Font.custom(FontName.SFProText.regular.description, size: 17)
    static let SFProTextRegular18 = Font.custom(FontName.SFProText.regular.description, size: 18)
    static let SFProTextRegular19 = Font.custom(FontName.SFProText.regular.description, size: 19)
    static let SFProTextRegular20 = Font.custom(FontName.SFProText.regular.description, size: 20)
    static let SFProTextRegular21 = Font.custom(FontName.SFProText.regular.description, size: 21)
    static let SFProTextRegular22 = Font.custom(FontName.SFProText.regular.description, size: 22)
    static let SFProTextRegular23 = Font.custom(FontName.SFProText.regular.description, size: 23)
    static let SFProTextRegular24 = Font.custom(FontName.SFProText.regular.description, size: 24)
    static let SFProTextRegular25 = Font.custom(FontName.SFProText.regular.description, size: 25)
    
    static let SFProTextSemibold12 = Font.custom(FontName.SFProText.semiBold.description, size: 12)
    static let SFProTextSemibold13 = Font.custom(FontName.SFProText.semiBold.description, size: 13)
    static let SFProTextSemibold14 = Font.custom(FontName.SFProText.semiBold.description, size: 14)
    static let SFProTextSemibold15 = Font.custom(FontName.SFProText.semiBold.description, size: 15)
    static let SFProTextSemibold16 = Font.custom(FontName.SFProText.semiBold.description, size: 16)
    static let SFProTextSemibold17 = Font.custom(FontName.SFProText.semiBold.description, size: 17)
    static let SFProTextSemibold18 = Font.custom(FontName.SFProText.semiBold.description, size: 18)
    static let SFProTextSemibold19 = Font.custom(FontName.SFProText.semiBold.description, size: 19)
    static let SFProTextSemibold20 = Font.custom(FontName.SFProText.semiBold.description, size: 20)
    static let SFProTextSemibold21 = Font.custom(FontName.SFProText.semiBold.description, size: 21)
    static let SFProTextSemibold22 = Font.custom(FontName.SFProText.semiBold.description, size: 22)
    static let SFProTextSemibold23 = Font.custom(FontName.SFProText.semiBold.description, size: 23)
    static let SFProTextSemibold24 = Font.custom(FontName.SFProText.semiBold.description, size: 24)
    static let SFProTextSemibold25 = Font.custom(FontName.SFProText.semiBold.description, size: 25)
    
    
    static let PoppinsMedium12 = Font.custom(FontName.Poppins.medium.description, size: 12)
    static let PoppinsMedium13 = Font.custom(FontName.Poppins.medium.description, size: 13)
    static let PoppinsMedium14 = Font.custom(FontName.Poppins.medium.description, size: 14)
    static let PoppinsMedium15 = Font.custom(FontName.Poppins.medium.description, size: 15)
    static let PoppinsMedium16 = Font.custom(FontName.Poppins.medium.description, size: 16)
    static let PoppinsMedium17 = Font.custom(FontName.Poppins.medium.description, size: 17)
    static let PoppinsMedium18 = Font.custom(FontName.Poppins.medium.description, size: 18)
    static let PoppinsMedium19 = Font.custom(FontName.Poppins.medium.description, size: 19)
    static let PoppinsMedium20 = Font.custom(FontName.Poppins.medium.description, size: 20)
    static let PoppinsMedium21 = Font.custom(FontName.Poppins.medium.description, size: 21)
    static let PoppinsMedium22 = Font.custom(FontName.Poppins.medium.description, size: 22)
    static let PoppinsMedium23 = Font.custom(FontName.Poppins.medium.description, size: 23)
    static let PoppinsMedium24 = Font.custom(FontName.Poppins.medium.description, size: 24)
    static let PoppinsMedium25 = Font.custom(FontName.Poppins.medium.description, size: 25)
    
    
    static let PoppinsBold12 = Font.custom(FontName.Poppins.bold.description, size: 12)
    static let PoppinsBold13 = Font.custom(FontName.Poppins.bold.description, size: 13)
    static let PoppinsBold14 = Font.custom(FontName.Poppins.bold.description, size: 14)
    static let PoppinsBold15 = Font.custom(FontName.Poppins.bold.description, size: 15)
    static let PoppinsBold16 = Font.custom(FontName.Poppins.bold.description, size: 16)
    static let PoppinsBold17 = Font.custom(FontName.Poppins.bold.description, size: 17)
    static let PoppinsBold18 = Font.custom(FontName.Poppins.bold.description, size: 18)
    static let PoppinsBold19 = Font.custom(FontName.Poppins.bold.description, size: 19)
    static let PoppinsBold20 = Font.custom(FontName.Poppins.bold.description, size: 20)
    static let PoppinsBold21 = Font.custom(FontName.Poppins.bold.description, size: 21)
    static let PoppinsBold22 = Font.custom(FontName.Poppins.bold.description, size: 22)
    static let PoppinsBold23 = Font.custom(FontName.Poppins.bold.description, size: 23)
    static let PoppinsBold24 = Font.custom(FontName.Poppins.bold.description, size: 24)
    static let PoppinsBold25 = Font.custom(FontName.Poppins.bold.description, size: 25)
}


public struct Constants {
    static let APP_NAME = "KICKDRILL"
    static let LOGIN_FAIL = "Login Failed"
    static let SIGNUP_FAIL = "Signup Failed"
    static let SESSION_EXPIRED = "Session Expired"
    
    
    static let NO_INTERNET = "No Internet Connection"
    static let SOMETHING_WENT_WRONG = "Something went wrong"
    static let ENTER_EMAIL = "Enter E-mail address"
    static let ENTER_VALID_EMAIL = "Enter Valid E-mail address"
    static let ENTER_PASSWORD = "Enter Password"
    static let ENTER_FULLNAME = "Enter your full name"
    static let ENTER_VALID_NUMBER = "Enter valid mobile number"
    static let TICK_CHECKBOX = "Please Accept terms and condition"
    static let ENTER_OLD_PASSWORD = "Enter your current password"
    static let ENTER_NEW_PASSWORD = "Enter new password"
    static let ENTER_CONFIRM_PASSWORD = "Enter confirm password"
    static let MATCH_CONFIRM_PASSWORD = "Confirm password should be same as new password"
    static let ENTER_VALID_COMPLAIN = "Enter valid complain or suggestions"
    static let ACCEPT_POCLICIES = "Please accept the policy to use safetyscope."
    static let AWS_ACCESSKEY = "AKIA4EKGWHESGTKDIZKL"
    static let AWS_SECRETKEY_ID = "VrQsMIneKaa/r8CDEmY+yFlMs2LlgKme8CHZoR0w"
    static let AWS_S3_BUCKETNAME = "safetyscope"

}

/// key for userdefualt
public struct UserDefaultsKey {
    static let kRememberName                     = "rememberName"
    static let kRememberEmail                    = "rememberEmail"
    static let kIsLogin                          = "isLogin"
    static let kIsLogout                         = "isLogout"
    static let kLanguageId                       = "LanguageId"
    static let kAccessToken                      = "AccessToken"
    static let kAuthToken                        = "AuthToken"
    static let kProfileImage                     = "ProfileImage"
    static let kProfileName                      = "ProfileName"
    static let kUserId                           = "userId"
    static let klastSyncDate                     = "lastSyncDate"
    static let klastSyncDateTime                 = "lastSyncDateTime"
    static let kSpecialSubject                   = "specialSubject"
    static let kCategoryId                       = "categoryId"
    static let kSubCategoryId                    = "subCategoryId"
    static let kExamCategoryId                   = "examCategoryId"
    static let kExamSubCategoryId                = "examSubCategoryId"
    static let kPracticeBasicInfo                = "practiceBasicInfo"
}

public struct Language {
    static let english                     = "en"
}

public struct Device_Type {
    static let ios                         = "IOS"
}


struct Keys {
    static let lang                         = "lang"
    static let email                        = "email"
    static let password                     = "password"
    static let name                         = "name"
    static let device_type                  = "device_type"
    static let mobile                       = "mobile"
    static let apnstoken                    = "apnstoken"
    static let verification_code            = "verification_code"
    static let page_name                    = "page_name"
    static let old_password                 = "old_password"
    static let new_password                 = "new_password"
    static let user_id                      = "user_id"
    static let user_img                     = "user_img"
    static let last_sync_date_time          = "last_sync_date_time"
    static let cat_type                     = "cat_type"
    static let category_id                  = "category_id"
    static let subcategory_id               = "subcategory_id"
    static let exam_data                    = "exam_data"
    static let question_id                  = "question_id"
    static let answer                       = "answer"
    static let data                         = "data"
    static let add_to_statistics_status        = "add_to_statistics_status"
}

struct Variables {
    static let email = "email"
    static let phone_number = "phone_number"
    static let name = "name"
    static let picture = "picture"
    static let user_id = "user_id"
    static let country_code = "country_code"
    static let deviceToken = "deviceToken"
    static let date_format = "date_format"
}

struct PageName {
    static let privacy_policy = "privacy_policy"
    static let about_us_content = "about_us_content"
    static let terms_conditions__content = "terms_conditions__content"
    static let contact_us = "contact_us"
    static let si_api = "si_api"
    static let practical = "practical"
    static let key_info = "key_info"
    static let killsheet = "killsheet"
}


///Restriction message
public struct RestrictionMessage {
    static let areYouSureToSubmit          = "are_you_sure_to_submit".localizedString()
    static let areYouSureToFinish          = "are_you_sure_to_finish".localizedString()
    static let areYouSureToLogout          = "are_you_sure_logout".localizedString()
    static let areYouSureToDeleteAccount   = "are_you_sure_delete_account".localizedString()
    static let areYouSureToClearResult     = "are_you_sure_clear_result".localizedString()
    static let areYouSureToDeleteResult    = "are_you_sure_delete_result".localizedString()
    static let areYouReadyToTakeExam       = "are_you_ready_to_take_exam".localizedString()
    static let areYouReadyToStartPractice  = "are_you_ready_to_start_practice".localizedString()
    static let examTimeFinished            = "exam_time_finished".localizedString()
    static let cancelExam                  = "cancel_exam".localizedString()
    static let cancelPractice              = "cancel_practice".localizedString()
    static let passwordMustBeAtLeastLong   = "include_both_lower_upper_case".localizedString()
    static let alreadyPurchasedSubscription = "already_purchased_subscription".localizedString()
    static let attemptedAllQstnsCorrect   = "attempted_all_questions_correctly".localizedString()
    static let notAttemptedAnyQuestion   = "not_attempted_question".localizedString()
}

public struct ButtonText {
    static let yes:String                   = "yes".localizedString()
    static let no:String                    = "no".localizedString()
    static let ok:String                    = "ok".localizedString()
    static let cancel:String                = "cancel".localizedString()
    static let submit:String                = "submit".localizedString()
    static let review:String                = "review".localizedString()
    static let finish:String                = "finish".localizedString()
    static let start:String                 = "start".localizedString()
    static let bcontinue:String              = "continue".localizedString()
    static let update:String                 = "update".localizedString()
    static let later:String                 = "later".localizedString()
}


struct FontName {
    enum SFProText : CustomStringConvertible {
        
        case regular
        case semiBold
        
        var description: String {
            switch self {
            case .semiBold:
                return "SFProText-Semibold"
            case .regular:
                return "SFProText-Regular"
            }
        }
    }
    
    enum Poppins : CustomStringConvertible {
        case black
        case blackItalic
        case bold
        case boldItalic
        case extraBold
        case extraBoldItalic
        case extraLight
        case extraLightItalic
        case italic
        case light
        case lightItalic
        case medium
        case mediumItalic
        case regular
        case semiBold
        case semiBoldItalic
        case thin
        case thinItalic
        
        var description: String {
            switch self {
            case .black:
                return "Poppins-Black"
            case .blackItalic:
                return "Poppins-BlackItalic"
            case .bold:
                return "Poppins-Bold"
            case .boldItalic:
                return "Poppins-BoldItalic"
            case .extraBold:
                return "Poppins-ExtraBold"
            case .extraBoldItalic:
                return "Poppins-ExtraBoldItalic"
            case .extraLight:
                return "Poppins-ExtraLight"
            case .extraLightItalic:
                return "Poppins-ExtraLightItalic"
            case .italic:
                return "Poppins-Italic"
            case .light:
                return "Poppins-Light"
            case .lightItalic:
                return "Poppins-LightItalic"
            case .medium:
                return "Poppins-Medium"
            case .mediumItalic:
                return "Poppins-MediumItalic"
            case .regular:
                return "Poppins-Regular"
            case .semiBold:
                return "Poppins-SemiBold"
            case .semiBoldItalic:
                return "Poppins-SemiBoldItalic"
            case .thin:
                return "Poppins-Thin"
            case .thinItalic:
                return "Poppins-ThinItalic"
            }
        }
    }
}

struct CommonModel : Codable {
    let status : Bool?
    let message : String?
    let data : JSONAny?
}

struct Colors {
    static let BorderColor  = Color(red:0.80, green:0.80, blue:0.80)
    static let WhiteColor   = Color.white
    static let BlackColor   = Color.black
    static let PrimaryBlue  = Color(red:0.00, green:0.67, blue:0.93)
    static let ClearColor   = Color.clear
    static let GreenColor   = Color(red:0.35, green:0.80, blue:0.31)
    static let GreyColor    = Color.gray
    
    
    static let AppColor     = Color(red: 37.0/255.0, green: 113.0/255.0, blue: 139.0/255.0)
    static let NavColor     = Color(red: 64.0/255.0, green: 143.0/255.0, blue: 170.0/255.0)
    static let TxtBackgroundColor = Color(red: 158.0/255.0, green: 158.0/255.0, blue: 158.0/255.0)
    static let TxtTopPlaceholderColor = Color(red: 101.0/255.0, green: 101.0/255.0, blue: 101.0/255.0)
    static let TxtGrayColor = Color(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0)
    static let TxtBorderGrayColor = Color(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0)
    static let BackgroundGrayColor = Color(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0)
    static let SplashBackgroundColor = Color(red: 222.0/255.0, green: 246.0/255.0, blue: 254.0/255.0)
    static let ViewShadowGrayColor = Color(red: 119.0/255.0, green: 119.0/255.0, blue: 119.0/255.0)
    static let TxtRedColor = Color(red: 241.0/255.0, green: 1.0/255.0, blue: 1.0/255.0)
    static let TxtGreenColor = Color(red: 0.0/255.0, green: 123.0/255.0, blue: 77.0/255.0)
    static let BackgroundBlueColor = Color(red: 241.0/255.0, green: 251.0/255.0, blue: 255.0/255.0)
    static let PieChartRedColor = Color(red: 234.0/255.0, green: 82.0/255.0, blue: 78.0/255.0)
    static let PieChartGreenColor = Color(red: 45.0/255.0, green: 127.0/255.0, blue: 102.0/255.0)
    static let SplashBackgrndColor = Color(red: 226.0/255.0, green: 248.0/255.0, blue: 255.0/255.0)
}

public extension Notification.Name {
    static let sessionStatusChanged = Notification.Name("SessionStatusChanged")
}

extension UserDefaults{
    enum keys {
        struct User {
            static let user_model = "user_model"
        }
    }
}
