//
//  Headers.swift
//  MCQ
//
//  Created by cis on 25/02/22.
//

import UIKit

class Headers: NSObject {
    func header() -> [String : String] {
        let header = ["Accept": "application/json"]
        return header
    }
    
    func tokenHeader() -> [String : String] {
        let header = ["Accept": "application/json", "API-ACCESS-TOKEN": "4942de482cf098fb1e0d5b6ba6dd2f06"]
        return header
    }
    
    func accessTokenHeader() -> [String : String] {
        let header = ["Accept": "application/json", "API-ACCESS-TOKEN": UserDefaults.standard.value(forKey: UserDefaultsKey.kAccessToken) as? String ?? ""]
        return header
    }
    
    func authTokenHeader() -> [String : String] {
        let header = ["Accept": "application/json", "API-ACCESS-TOKEN": UserDefaults.standard.value(forKey: UserDefaultsKey.kAccessToken) as? String ?? "", "Authorization" : "Bearer \(UserDefaults.standard.value(forKey: UserDefaultsKey.kAuthToken) as? String ?? "")"]
        return header
    }
}

