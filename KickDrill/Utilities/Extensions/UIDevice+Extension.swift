//
//  UIDevice+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension UIDevice {
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
}
extension UIViewController {
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    func getTimeNDateFromTimeStamp(stamp:Int, outFormat:String) -> String {
        let dateFormatter = DateFormatter()
        let sdate = Date(timeIntervalSince1970: TimeInterval(stamp))
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = outFormat//"hh:mm a"//"yyyy, MMM d, h:mm a"
        let timeStamp = dateFormatter.string(from: sdate)
        return timeStamp
    }
    func isValidPhone(phone: String) -> Bool {
        
        let phoneRegex = "^[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
}
