//
//  String+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    /// Make substring to underline
    ///
    /// - Parameter substring: substring
    /// - Returns: attributed string with underline substring
    func underline(toSubstring substring:String, foregroundColor: UIColor, underlineColor: UIColor) -> NSMutableAttributedString  {
        let attributedString = NSMutableAttributedString(string: self)
        let substringRange = attributedString.mutableString.range(of:substring)
        let attributes = [NSAttributedString.Key.foregroundColor:foregroundColor, NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor:underlineColor] as [NSAttributedString.Key : Any]
        attributedString.addAttributes(attributes, range: substringRange)
        return attributedString
        
    }
}

extension String {
    //----------------------------------------------------------------------------------------
    // Mark: Localization
    //----------------------------------------------------------------------------------------
    /// Return localizse string for key
    ///
    /// - Returns: Return localize string
    func localizedString() ->String
    {
        let deviceLanguage:String = Locale.current.languageCode!
        let localizeString = getLocalizeValue(deviceLanguage: deviceLanguage)
        return localizeString == self ? getLocalizeValue(deviceLanguage: "en") : localizeString
    }
    
    func getLocalizeValue(deviceLanguage:String) -> String
    {
        let defaultAppLanguage = "en" //english
        //Getting path of string file
        let path = Bundle.main.path(forResource: deviceLanguage, ofType: "lproj") != nil ?
            Bundle.main.path(forResource: deviceLanguage, ofType: "lproj") :
            Bundle.main.path(forResource: defaultAppLanguage , ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension NSMutableAttributedString {
    func setColorForText(textToFind: String, withColor color: UIColor, withFont font: UIFont) {
         let range: NSRange = self.mutableString.range(of: textToFind, options: .caseInsensitive)
        if range != nil {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 3
            self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
            self.addAttribute(NSAttributedString.Key.font, value: font, range: range)
            self.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
          }
        }

}

extension String {
    //----------------------------------------------------------------------------------------
    // Mark: Email Validation
    //----------------------------------------------------------------------------------------
    /// Check email is valid or not
    ///
    /// - Returns: True if valid else false
    func isValidEmail() -> Bool {
        print("validate emilId: \(self)")
        let emailRegEx = "^[A-Z0-9a-z._%\\+\\-]+@[A-Za-z0-9\\.\\-]+\\.[A-Za-z]{2,4}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
    
    func isValidPhoneNumber() -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result
    }
    
    func isValidName() -> Bool {
        let nameRegex = "^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        let result =  nameTest.evaluate(with: self)
        return result
        
    }
    
    func containsSpecialCharacters() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[^a-z0-9 ]", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) {
                return true
            } else {
                return false
            }
        } catch {
            debugPrint(error.localizedDescription)
            return true
        }
    }
    
    
    /// Check space contain
    ///
    /// - Returns: True if valid else false
    func isContainSpace() -> Bool {
        return (self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    func isValidEnteredPassword() -> Bool {
       // let passRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*#?^*.\\[\\]\\{\\}\\(\\)\\?\\-\"/,><':;|_~`&\\\\])[A-Za-z\\d$@$!%*#?^*.\\[\\]\\{\\}\\(\\)\\?\\-\"/,><':;|_~`&\\\\]{6,}$"
        let passRegex = "(?=[^A-Z]*[A-Z])[^0-9]*[0-9].*"
        let passTest = NSPredicate(format: "SELF MATCHES %@", passRegex)
        let result = passTest.evaluate(with: self)
        return result
    }
    
}

extension String {
    public var convertHtmlToNSAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data,options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func convertHtmlToAttributedStringWithCSS(font: UIFont? , csscolor: String , lineheight: Int, csstextalign: String) -> NSAttributedString? {
        guard let font = font else {
            return convertHtmlToNSAttributedString
        }
        let modifiedString = "<style>body{font-family: '\(font.fontName)'; font-size:\(font.pointSize)px; color: \(csscolor); line-height: \(lineheight)px; text-align: \(csstextalign); }</style>\(self)";
        guard let data = modifiedString.data(using: .utf8) else {
            return nil
        }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
        catch {
            print(error)
            return nil
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font : font], context: nil)
        return ceil(boundingBox.height)
    }
}

extension NSAttributedString {
    func withLineSpacing(_ spacing: CGFloat) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(attributedString: self)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = spacing
        attributedString.addAttribute(.paragraphStyle,
                                      value: paragraphStyle,
                                      range: NSRange(location: 0, length: string.count))
        return NSAttributedString(attributedString: attributedString)
    }
}
