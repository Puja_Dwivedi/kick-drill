//
//  UIImageView_UIImage+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit
import Kingfisher

extension UIImageView {}

extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func isEqualToImage(_ image: UIImage) -> Bool {
        let data1 = self.pngData()
        let data2 = image.pngData()
        return data1 == data2
    }
}

extension UIImageView {
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}

extension UIImageView {
    //-------------------------------------------------------------------------
    //MARK: - Download Image
    //-------------------------------------------------------------------------
    
    /// Download image from URL
    ///
    /// - Parameters:
    ///   - strURL: URL to download image in string
    ///   - placeholderImage: placholder image name
    ///   - activityType: activity indicator
    
    
    func downloadImage(strURL:String, placeholderImage:UIImage?, placeholderContentMode:UIView.ContentMode? = .center, activityType:IndicatorType)
    {
        //getting cloud front url
        // let imageURL = getCloudFrontURLof(strURL: strURL)
        
        ///Convert String to url
        let url = URL(string: strURL)
        
        DispatchQueue.main.async {
            // set placeholder image
            self.image = placeholderImage
        }
        
        // initail content mode
        self.contentMode =  placeholderContentMode!
        self.kf.setImage(with: url, placeholder: placeholderImage) { result in
            switch result {
            case .success(let value):
                DispatchQueue.main.async {
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                    self.image = value.image
                    self.contentMode = .scaleAspectFill
                    self.clipsToBounds = true
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    print("Error: \(error)")
                    self.image = placeholderImage
                }
            }
        }
//        self.kf.setImage(with: url, placeholder: placeholderImage, completionHandler:  { (image, error, cacheType, imageURL) in
//            DispatchQueue.main.async {
//                if let image = image {
//                    print("Image: \(image). Got from: \(cacheType)")
//                    self.image = image
//                    self.contentMode = .scaleAspectFill
//                    self.clipsToBounds = true
//                }
//            case .failure(let error):
//                DispatchQueue.main.async {
//                    print("Error: \(error)")
//                    self.image = placeholderImage
//                }
//            }
//        })
    }
}
