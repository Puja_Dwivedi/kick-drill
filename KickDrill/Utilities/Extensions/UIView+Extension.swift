//
//  UIView+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension UIView{
    @IBInspectable var cornerRadius: CGFloat {
        
        get{
            return layer.cornerRadius
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.cornerRadius = newValue
                self.layer.masksToBounds = newValue > 0
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderWidth = newValue
            }
        }
    }
    
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderColor = newValue.cgColor
            }
        }
    }
    
    func setViewCorners() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.clipsToBounds = true
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.maskedCorners = corners
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    func addShadowView(shadowColor: CGColor, cornerRadius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2) //(width: 0.5, height: 4.0)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
    }
    
    func addShadowWithRoundedCorners(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white) {
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
      //  let bnds = CGRect(x: 0.0, y: 0.0, width: self.bounds.width-40.0, height: self.bounds.height)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
    
    func addShadowTo(radious : CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: UIRectCorner.bottomLeft.union(.bottomRight), cornerRadii: CGSize(width: 6, height: 6)).cgPath
        self.layer.shadowPath = maskPath
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 2
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath
        self.layer.mask = maskLayer
    }
    
    func addShadowToPieChart(shadowColor: CGColor, innerView: UIView) {
        self.layer.masksToBounds = false
      //  self.layer.cornerRadius = self.layer.bounds.height/2 - 10.0
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 6.0
        
        innerView.layer.cornerRadius = innerView.bounds.height/2
        innerView.layer.borderWidth = 0.1
        innerView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func addShadowViewWithCorners(shadowColor: CGColor, innerView: UIView) {
        self.layer.masksToBounds = false
      //  self.layer.cornerRadius = self.layer.bounds.height/2 - 10.0
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 6.0
        
        innerView.layer.cornerRadius = innerView.bounds.height/2 - 5
        innerView.layer.borderWidth = 0.1
        innerView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func addShadowViewWithCornersForView(shadowColor: CGColor, innerView: UIView) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 20.0
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 6.0
        
        innerView.layer.cornerRadius = 20.0
        innerView.layer.borderWidth = 0.1
        innerView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    /// This method used to make lable Circle
    func makeCircle()  {
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func setNoDataView(title: String, message: String, topBottomMargin : CGFloat) {
        let emptyView = UIView(frame: CGRect(x: 0, y: topBottomMargin/2, width: self.bounds.size.width, height: self.bounds.size.height - 2*topBottomMargin))
        emptyView.tag = 2002
        let titleLabel = UILabel.init(frame: CGRect(x: 5, y: (self.bounds.size.height - 2*topBottomMargin)/2-20, width: self.bounds.size.width-10, height: 40))
        let messageLabel = UILabel.init(frame: CGRect(x: 5, y: titleLabel.frame.maxY + 10, width: self.bounds.size.width-10, height: 60))
        titleLabel.text = title
        messageLabel.text = message
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "Poppins-Bold", size: 17)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        emptyView.center = self.center
        self.addSubview(emptyView)
    }
    func restoreView() {
        for view in self.subviews {
            if view.tag == 2002{
                view.removeFromSuperview()
                return
            }
        }
    }
}
@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override public class var layerClass: AnyClass { CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }
}

/////----------
//MARK:- IBInspectable
extension UIView {

    @IBInspectable
    var shadowRadiuss: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.masksToBounds = false
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable
    var shadowOpacityy: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.masksToBounds = false
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable
    var shadowOffsett: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.masksToBounds = false
            layer.shadowOffset = newValue
        }
    }

    @IBInspectable
    var shadowColorr: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UIView {
    func snapshot(of rect: CGRect? = nil, afterScreenUpdates: Bool = true) -> UIImage {
        return UIGraphicsImageRenderer(bounds: rect! ).image { _ in
            drawHierarchy(in: bounds, afterScreenUpdates: true)
        }
    }
}


extension UIView {
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}


extension UISwitch {
    func set(width: CGFloat, height: CGFloat) {
        let standardHeight: CGFloat = 31
        let standardWidth: CGFloat = 51
        let heightRatio = height / standardHeight
        let widthRatio = width / standardWidth
        transform = CGAffineTransform(scaleX: widthRatio, y: heightRatio)
    }
}
