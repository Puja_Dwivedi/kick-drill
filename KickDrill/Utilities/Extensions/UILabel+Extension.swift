//
//  UILabel+Extensions.swift
//  MCQ
//
//  Created by cis on 13/01/22.
//

import Foundation
import UIKit

//----------------------------------------------------------------------------------------
// MARK:- Extension Methods
//----------------------------------------------------------------------------------------
extension UILabel
{
    
    /// Rotat lable in perticular angle
    @IBInspectable var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
    
    @IBInspectable var localizationKey: String {
        get {
            return ""
        } set {
            self.text = newValue.localizedString()
        }
    }
    
//    /// This method used to make lable Circle
//    func makeCircle()  {
//        self.layer.cornerRadius = self.frame.width/2
//        self.layer.masksToBounds = true
//    }
    
    /// This method used to set border of lable
    ///
    /// - Parameters:
    ///   - width: Width of border
    ///   - color: Color of boder
    func addBorder(width:CGFloat, color:UIColor)  {
        //set Border
        self.layer.borderWidth = width
        // set color
        self.layer.borderColor = color.cgColor
    }
    
    
    /// Calculate height of lable
    ///
    /// - Parameters:
    ///   - text: test
    ///   - font: font
    ///   - width: Width
    /// - Returns: heigth
    func heightForLabel(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    //Not perfect method to sub & super script
    /// Subcript a perticular string
    ///
    /// - Parameters:
    ///   - substring: subscript string
    ///   - fontSub: font for subscript text
    func setSubscript(substring:String, fontSub:UIFont, mainFont:UIFont  )  {
        let attString:NSMutableAttributedString = isAttributed ? self.attributedText as! NSMutableAttributedString : NSMutableAttributedString(string: self.text!, attributes: [.font:mainFont])
        let range = (self.text! as NSString).range(of:substring)
        attString.setAttributes([.font:fontSub,.baselineOffset:-2], range: range)
        self.attributedText = attString
    }
    
    /// Subcript a perticular string
    ///
    /// - Parameters:
    ///   - superstring: superscript string
    ///   - fontSuper: font for superscript text
    func setSuperscript(superstring:String, fontSuper:UIFont , mainFont:UIFont)  {
        let attString:NSMutableAttributedString = isAttributed ? self.attributedText as! NSMutableAttributedString : NSMutableAttributedString(string: self.text!, attributes: [.font:mainFont])
        let range = (self.text! as NSString).range(of:superstring)
        attString.setAttributes([.font:fontSuper,.baselineOffset:12], range: range)
        self.attributedText = attString
    }
    
   
    /// Check string have attributed text or not
    var isAttributed: Bool {
        guard let attributedText = attributedText else { return false }
        let range = NSMakeRange(0, attributedText.length)
        var allAttributes = [Dictionary<NSAttributedString.Key, Any>]()
        attributedText.enumerateAttributes(in: range, options: []) { attributes, _, _ in
            allAttributes.append(attributes)
        }
        return allAttributes.count > 1
    }
    
    func underlineText(labelText: String, underlineColor: UIColor, foregroundColor: UIColor) -> NSAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: labelText)
        attributeString.addAttributes([NSAttributedString.Key.underlineStyle:NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor:underlineColor, NSAttributedString.Key.foregroundColor:foregroundColor], range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
}


