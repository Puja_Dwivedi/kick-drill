//
//  UIApplication+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension UIApplication{
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.windows[0].rootViewController) -> UIViewController?{
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
    
    class func getTopViewController() -> UIViewController{
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return topViewController()!
    }
}

