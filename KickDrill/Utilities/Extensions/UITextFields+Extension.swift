//
//  UITextFields+Extension.swift
//  MCQ
//
//  Created by Apple on 04/01/22.
//

import UIKit

extension UITextField {
   @IBInspectable var localizedPlaceholder: String {
       get {
           return ""
       }
       set {
           self.placeholder = newValue
       }
   }
   
   @IBInspectable var placeHolderColor: UIColor? {
       get {
           return self.placeHolderColor
       }
       set {
           self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[.foregroundColor: newValue!])
       }
   }
   
   @IBInspectable var localizedText: String {
       get {
           return ""
       }
       set {
           self.text = newValue
       }
   }
   
   @IBInspectable var Padding : CGFloat{
       get {
           return 0.0
       }
       set{
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.size.height))
           self.leftView = paddingView
           self.leftViewMode = .always
       }
   }
   func setLeftIcon(_ icon: UIImage) {
       
       let padding = 10
       let size = 15
       
       let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
       let iconView  = UIImageView(frame: CGRect(x: 5, y: 0, width: size, height: size))
       iconView.image = icon
       iconView.contentMode = .scaleAspectFit
       outerView.addSubview(iconView)
       
       leftView = outerView
       leftViewMode = .always
   }
    
    /// Set Localize placeholder
    ///
    /// - Parameter localize key: String
    func setLocalizePlaceholder(localizeKey:String) {
       self.placeholder = localizeKey.localizedString()
    }
}

extension UITextField {
    func setBorder(borderWidth: CGFloat, cornerRadius: CGFloat, borderColor: UIColor) {
        self.borderWidth = borderWidth
        self.cornerRadius = cornerRadius
        self.borderColor = borderColor
    }
}
