//
//  UIButton+Extension.swift
//  MCQ
//
//  Created by cis on 11/01/22.
//

import UIKit

extension UIButton {
    /// Underline button title or substring if substring == "" then complete title underline
    ///
    /// - Parameter substring: String for underline
    func underline(toSubstring substring:String, foregroundColor: UIColor, underlineColor: UIColor)  {
        //check title
        guard (self.titleLabel?.text?.count)! > 0 else { return }
        //check and set string to undeline
        let strSubString:String = substring.count == 0 ? (self.titleLabel?.text)! :substring
        let str:String = (self.titleLabel?.text)!
        //get attribute string with underline method written on string extenstion
        let attributedString = str.underline(toSubstring: strSubString, foregroundColor: foregroundColor, underlineColor: underlineColor)
        //set String to button
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    @IBInspectable var localizationKey: String {
        get {
            return ""
        } set {
            self.titleLabel?.text = newValue.localizedString()
        }
    }
    
}

extension UIButton {
    func buttonRoundCorners(_ corners: CACornerMask, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.maskedCorners = corners
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
    
    func setButtonCorners() {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.clipsToBounds = true
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
}
