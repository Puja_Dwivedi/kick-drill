//
//  UIViewController+Extension.swift
//  MCQ
//
//  Created by cis on 12/01/22.
//

import UIKit
//import Charts
import WebKit

extension UIViewController {
    // return viewcontroller identifire
    static var identifier : String {
        return String.init(describing: self)
    }
    
    func setLabelWithDifferentFonts(lblInitialValue: String, lblFinalValue: String, initialFont: UIFont, finalFont: UIFont) -> NSMutableAttributedString {
        let attrsA = [NSAttributedString.Key.font: initialFont]
        let a = NSMutableAttributedString(string:lblInitialValue, attributes:attrsA as [NSAttributedString.Key : Any])
        let attrsB =  [NSAttributedString.Key.font: finalFont]
        let b = NSAttributedString(string:lblFinalValue, attributes:attrsB as [NSAttributedString.Key : Any])
        a.append(b)
        return a
    }
    
}

//-------------------------------------------------------------------------
//MARK: - Alert
//-------------------------------------------------------------------------
extension UIViewController {
    // Remove this method
    func showWorkInProgress()
    {
        showAlertWith("", message: "Work in progress", buttons: ["OK"]) { (index) in }
    }
    
    /// Show alert view controller
    ///
    /// - Parameters:
    ///   - title: Title for alert
    ///   - message: Message for alert
    ///   - buttons: array of button
    ///   - tapBlock: handle touch event on button
    func showAlertWith(_ title: String, message: String, buttons:[String], tapBlock:((Int) -> Void)?)
    {
        //Main thread
        DispatchQueue.main.async(execute: {
            //alert instance
            let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
            //add button on alert
            for strName in buttons
            {
                alert.addAction(UIAlertAction(title: strName, style: .default, handler:{ action in
                    tapBlock!(buttons.firstIndex(of: strName)!)
                }))
            }
            self.present(alert, animated: true)
        })
    }
    
    /// Show alert which dismiss after 5 sec
    ///
    /// - Parameters:
    ///   - title: Title for alert
    ///   - message: Message for alert
    func showAutoDismissAlert(title: String, message: String) {
        // The alert view
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        //Main Thread
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(5.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            //dismiss alert after 5 second
            alert.dismiss(animated: true, completion: {() -> Void in
            })
        })
    }
    
    func alertControllerDismissed()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Show alert which dismiss after given seconds
    ///
    /// - Parameters:
    ///   - time: time to show alert
    ///   - title: Title for alert
    ///   - message: Message for alert
    func showAutoDismissAlertAfter(time:Double, title: String, message: String) {
        // The alert view
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        //Main Thread
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
            //dismiss alert after 5 second
            alert.dismiss(animated: true, completion: {() -> Void in
            })
        })
    }
}

extension UIViewController {
    func menuOpen() {
//        self.frostedViewController.view.endEditing(true)
//        self.frostedViewController.backgroundFadeAmount = 0.0
//        self.frostedViewController.presentMenuViewController()
    }
}

extension UIScrollView {

    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x:0, y:childStartPoint.y,width: 1,height: self.frame.height), animated: animated)
        }
    }

    // Bonus: Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }

    // Bonus: Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }

}

extension Sequence where Element: AdditiveArithmetic {
    func sum() -> Element { reduce(.zero, +) }
}


extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}

extension UIViewController {
    func zoomWebviewContent(_ webView: WKWebView, zoomPercent: Int) {
        let javascript = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '\(zoomPercent)%'"
        webView.evaluateJavaScript(javascript) { (response, error) in
            print()
        }
    }
}
