//
//  ImageTypeView.swift
//  SpeakOn!
//
//  Created by User on 11/12/18.
//  Copyright © 2018 User. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

//----------------------------------------------------------------------------------------------
//MARK: - Protocol
//----------------------------------------------------------------------------------------------
protocol ImageTypeViewDelegate: class {
    func didUpdateView()
}

//----------------------------------------------------------------------------------------------
//MARK: - Class
//----------------------------------------------------------------------------------------------

class ImageTypeView: UIView {
    
    //----------------------------------------------------------------------------------------------
    //MARK: - Delegate
    //----------------------------------------------------------------------------------------------
    
    weak var delegate: ImageTypeViewDelegate?
    
    
    //----------------------------------------------------------------------------------------------
    //MARK: -IBoutlet
    //----------------------------------------------------------------------------------------------
    
    @IBOutlet weak var  containview:UIView!
    

    
    //----------------------------------------------------------------------------------------------
    //MARK: -IBoutlet
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //MARK: -Variable
    //----------------------------------------------------------------------------------------------
    
    var memoryTypeValue = 0  /// 1 audio  2 video 3 image
    
    var isProfilePic = false
    
    // To dismiss picker view on back button
    var isDismiss = false
    
    ///Maximum media to send server
    var maxMediaSelectionCount = 1
    
    /// Editing media count
    var editMediaCount = 0
    
    // contain selected images
    var arrImages:[UIImage] = []
    
    // true to allow croping
    var isAllowCroping:Bool = false
    
    // true for circular croping
    var isCircularCroping:Bool = false
    
    var count = 0
                
    //----------------------------------------------------------------------------------------------
    //MARK: - Overide Function
    //----------------------------------------------------------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commanInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commanInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    //----------------------------------------------------------------------------------------------
    //MARK: - Initial method
    //----------------------------------------------------------------------------------------------
    
    private func commanInit()
    {
        Bundle.main.loadNibNamed("ImageTypeView", owner: self, options: nil)
        addSubview(containview)
        containview.frame = self.bounds
        containview.autoresizingMask = [.flexibleHeight , .flexibleWidth]
    }
    
    //----------------------------------------------------------------------------------------------
    //MARK: - Action Button
    //----------------------------------------------------------------------------------------------
    
    /// Perform action on camera button touch
    ///
    /// - Parameter sender: UIbutton instance
    @IBAction func actionOnCamera(_ sender:UIButton)
    {
        openCamera()
    }
    
    /// Perform action on upload button touch
    ///
    /// - Parameter sender: UIbutton instance
    @IBAction func actionOnUpload(_ sender:UIButton)
    {
      //  openPicker(resourseType: .photos, sourceType: .photo)
    }
    
    //----------------------------------------------------------------------------------------------
    //MARK: - Multiple image Picker
    //----------------------------------------------------------------------------------------------
    
    /// Open Picker
    ///
    /// - Parameters:
    ///   - resourseType: Permission type camera/gallery
    ///   - sourceType: camera, photo, both
//    func openPicker(resourseType:PrivateResource, sourceType:DKImagePickerControllerSourceType)
//    {
//        //getting top controller
//        if let controller = UIApplication.topViewController()
//        {
//            self.maxMediaSelectionCount = 1
//            //Check Permission
//            let propose: Propose = {
//                proposeToAccess(resourseType, agreed: {
//                    // show picker
//                    let pickerController = DKImagePickerController()
//                    pickerController.singleSelect =  true
//                    pickerController.maxSelectableCount =  1
//                    pickerController.assetType = .allPhotos
//                    pickerController.sourceType = sourceType
//                    pickerController.didSelectAssets = { (assets: [DKAsset]) in
//                        // print(assets)
//                        self.count = 0
////                        showLoader(message: "")
//                        self.perform(#selector(self.fetchImageFromAsset(assets:)), with: assets, afterDelay: 0.3)
//                    }
//                    controller.present(pickerController, animated: true) {}
//                }, rejected: {
//                    //No photo available msg
//                    controller.alertNoPermissionToAccess(resourseType)
//                    self.moveToVC()
//                })
//            }
//            propose()
//        }
//    }
    
    /// get image from asset
//    @objc func fetchImageFromAsset(assets: [DKAsset])
//    {
//        if  assets.count == 0 {
//            //hideLoader()
//            moveToVC()
//            return
//        }
//
//        for asset in assets
//        {
//                asset.fetchOriginalImage(completeBlock: { (image, info) in
//                    self.arrImages.append(image!)
//                    let imgData: NSData = NSData(data: (image!).jpegData(compressionQuality: 1)!)
//                    let imageSize: Int = imgData.length
//                    print("size of image in MB: %f ", Double(imageSize) / 1024.0/1024.0)
//                    self.isAllowCroping == true ? self.presentCropViewController(img: self.arrImages[0]) : self.moveToVC()
////                    if let controller = UIApplication.topViewController()
////                    {
////                        UIViewController().dismiss(animated: true, completion: {
////                            self.isAllowCroping == true ? self.presentCropViewController(img: self.arrImages[0]) : self.moveToVC()
////                        })
////                    }
//                })
//        }
//    }
    
    /// Movet to Perticular controller of app
    func moveToVC()  {
        DispatchQueue.main.async {
           // hideLoader()
            self.delegate?.didUpdateView()
        }
    }
    
    func dismissVC() {
        if let controller = UIApplication.topViewController() {
            controller.dismiss(animated: true, completion: {
                self.moveToVC()
            })
        }
    }
    
    //----------------------------------------------------------------------------------------------
    //MARK: - Coustom Method
    //----------------------------------------------------------------------------------------------

}


//----------------------------------------------------------------------------------------------
//MARK: - Camera
//----------------------------------------------------------------------------------------------
extension ImageTypeView :UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func openCamera() {
        //getting top controller
        if let controller = UIApplication.topViewController()
        {
//            // check camera permission
//            let propose: Propose = {
//                proposeToAccess(.camera, agreed: {
//                    let imgPicker = UIImagePickerController()
//                    // 1 Check if project runs on a device with camera available
//                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                       
//                        // 2 Present UIImagePickerController to take video
//                        imgPicker.sourceType = .camera
//                        imgPicker.delegate = self
//                        imgPicker.allowsEditing = false
//                        
//                        DispatchQueue.main.async {
//                             controller.present(imgPicker, animated: true, completion: nil)
//                        }
//                    }
//                }, rejected: {
//                    //No photo available msg
//                    controller.alertNoPermissionToAccess(.camera)
//                    self.moveToVC()
//                })
//            }
//            propose()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
//        guard info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaType.rawValue)] != nil else { return }
//        if let controller = UIApplication.topViewController()
//        {
//            let image = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as! UIImage
//            self.arrImages.append(image)
//            let imgData: NSData = NSData(data: (image).jpegData(compressionQuality: 1)!)
//            let imageSize: Int = imgData.length
//            print("size of image in MB: %f ", Double(imageSize) / 1024.0/1024.0)
//            controller.dismiss(animated: true, completion: {
//                self.isAllowCroping == true ? self.presentCropViewController(img: self.arrImages[0]) : self.moveToVC()
//            })
//        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismissVC()
    }
    
}

//----------------------------------------------------------------------------------------------
// MARK: - Image Crop
//----------------------------------------------------------------------------------------------
//extension ImageTypeView : CropViewControllerDelegate {
//
//    /// Present crop view controller
//    ///
//    /// - Parameter img: Image
//    func presentCropViewController(img:UIImage)
//    {
//        //hideLoader()
//        self.isCircularCroping ? presentCropViewControllerAsCircle(img: img) : presentCropViewControllerAsRectangle(img:img)
//    }
//
//    /// Present crop view controller as rectangle Crop
//    ///
//    /// - Parameter img: Image
//    func presentCropViewControllerAsRectangle(img:UIImage) {
//        if let  controller = self.findViewController()
//        {
//            let cropViewController = CropViewController(image: img)
//            cropViewController.delegate = self
//
//            // Crop in 16:9  fixed aspect ratio
//            cropViewController.aspectRatioPreset = .preset16x9; //Set the initial aspect ratio as a square
//            cropViewController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
//            cropViewController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
//            cropViewController.aspectRatioPickerButtonHidden = true
//            controller.present(cropViewController, animated: true, completion: nil)
//        }
//    }
//
//    /// Present crop view controller as circular Crop
//    ///
//    /// - Parameter img: Image
//    func presentCropViewControllerAsCircle(img:UIImage) {
//        if let  controller = self.findViewController()
//        {
//            let cropViewController = CropViewController.init(croppingStyle: .circular, image: img)
//            cropViewController.delegate = self
//            controller.present(cropViewController, animated: true, completion: nil)
//        }
//    }
//
//    /// Crop view delgate method
//    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
//        // 'image' is the newly cropped version of the original image
//
//        if let  controller = self.findViewController()
//        {
//            //set image Selected
//            self.arrImages.removeAll()
//            self.arrImages.append(image)
//
//            // dismiss crop view
//            controller.dismiss(animated: true) {
//                self.moveToVC()
//            }
//        }
//    }
//
//    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
//        if let  controller = self.findViewController()
//        {
//            //set image Selected
//            self.arrImages.removeAll()
//
//            // dismiss crop view
//            controller.dismiss(animated: true) {
//                self.moveToVC()
//            }
//        }
//    }
//}

