//
//  ExamQstnHeaderView.swift
//  MCQ
//
//  Created by cis on 25/01/22.
//

import UIKit

class HintTitleView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btnHint: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }
    
    func setImage(isQstnHint: Bool) {
        isQstnHint == true ? (btnHint.setImage(UIImage.init(named: "qstnHint"), for: .normal)) : (btnHint.setImage(UIImage.init(named: "hint"), for: .normal))
    }
    
    
    func initSubviews() {
        let nib = UINib(nibName: "HintTitleView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
}
