//
//  CountryCodeView.swift
//  MyFirstApp
//
//  Created by cis on 31/12/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit

class CountryCodeModelClass {
    var countryFlag : String = ""
    var countryCode : String = ""
    var countryName : String = ""
    var countryNameWithCode : String = ""
    var max_phoneLength : Int = 0
    
    init(countryFlag : String, countryCode : String, countryName : String, countryNameWithCode : String,max_phoneLength : Int) {
        self.countryFlag = countryFlag
        self.countryCode = countryCode
        self.countryName = countryName
        self.countryNameWithCode = countryNameWithCode
        self.max_phoneLength = max_phoneLength
    }
}

class CountryCodeView: UIView{
      static let sharedInstance = CountryCodeView.initLoader()
      class func initLoader() -> CountryCodeView {
           return UINib(nibName: "CountryCodeView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CountryCodeView
       }
    
    //MARK:-
    //MARK:- IBOutlet
    
    @IBOutlet weak var txtFldSearch: UITextField!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var bottomSpaceCons: NSLayoutConstraint!
    @IBOutlet weak var btnCloseSearch: UIButton!
    @IBOutlet weak var rootView_H: NSLayoutConstraint!
    
    //MARK:-
    //MARK:- Variables :
    
    var onCloser : ((CountryCodeModelClass)-> Void)!
    var arrCountryList = [CountryCodeModelClass]()
    var arrSearchData = [CountryCodeModelClass]()
    var strSearch = ""
    
    //MARK:-
    //MARK:- App Flow
     
    override func awakeFromNib() {
        backgroundColor = UIColor.black.withAlphaComponent(0.8)
        txtFldSearch.delegate = self
        tblview.delegate = self
        tblview.dataSource =  self
        
        let nib = UINib.init(nibName: "CountryCodeCell", bundle: nil)
        self.tblview.register(nib, forCellReuseIdentifier: "CountryCodeCell")
        
        self.tblview.estimatedRowHeight = 60
        self.tblview.rowHeight = UITableView.automaticDimension
    }
    
    
    //MARK:-
    //MARK:- Main Method
        
    func Show(arr : [CountryCodeModelClass] ,onCompletion: @escaping (CountryCodeModelClass)-> Void){
        self.arrCountryList = arr
        self.arrSearchData = arr
        tblview.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 15))
        self.strSearch = ""
        self.frame = UIScreen.main.bounds
        self.onCloser = onCompletion
        self.rootView_H.constant = UIScreen.main.bounds.height/1.3
        
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(self)   //-->> add to subview
        btnCloseSearch.isHidden = true
        self.tblview.reloadData()
        self.showWithAnimation()
    }
    
    
    //MARK:-
    //MARK:- Action Methods :
    
    @IBAction func actionBack(_ sender: UIButton) {
        txtFldSearch.text = ""
        removeWithAnimation()
    }
    
    
    @IBAction func actionCloseSearch(_ sender: UIButton) {
        txtFldSearch.text = ""
        btnCloseSearch.isHidden = true
        arrSearchData = arrCountryList
        tblview.reloadData()
    }
    
     
     //MARK:-
     //MARK:- Methods :
     
         func showWithAnimation(){
             //============== Show with animation
             self.rootView.alpha = 0

            UIView.animate(withDuration: 0.7, animations: {
                self.rootView.alpha = 1
                self.bottomSpaceCons.constant = 0
                self.layoutIfNeeded()
             }) { (true) in }
         }

         func removeWithAnimation(){
          self.rootView.alpha = 1
            
            UIView.animate(withDuration: 0.7, animations: {
                self.rootView.alpha = 1
                self.bottomSpaceCons.constant = -UIScreen.main.bounds.height
                  self.layoutIfNeeded()
             }) { (true) in
                self.txtFldSearch.text = ""
                 self.removeFromSuperview()
             }
     }
    
    func removeWithAnimationWithValue(value : CountryCodeModelClass){
         self.rootView.alpha = 1
           
           UIView.animate(withDuration: 0.7, animations: {
               self.rootView.alpha = 1
               self.bottomSpaceCons.constant = -UIScreen.main.bounds.height
                 self.layoutIfNeeded()
            }) { (true) in
                self.txtFldSearch.text = ""
                self.removeFromSuperview()
                self.onCloser(value)
            }
    }
}


/*
 MARK:- textfield delegate for search element
 */
extension CountryCodeView :  UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty
           {
               strSearch = String(strSearch.dropLast())
           }
           else
           {
               strSearch=textField.text!+string
           }

          // print(strSearch)
            if strSearch.count > 0 {
                self.btnCloseSearch.isHidden = false
            }else{
             self.btnCloseSearch.isHidden = true
            }
           //let predicate = NSPredicate(format: "SELF.countryName CONTAINS[cd] %@", strSearch)
           let arrTempSearch = arrCountryList.filter {
            $0.countryName.range(of: "\(strSearch)", options: .caseInsensitive) != nil
           }
    
           if arrTempSearch.count > 0
           {
               arrSearchData.removeAll(keepingCapacity: true)
               arrSearchData = arrTempSearch
           }
           else
           {
               arrSearchData = arrCountryList
           }
           tblview.reloadData()
           return true
    }
}


/*
 MARK:- tableview delegates
 */
extension CountryCodeView :  UITableViewDelegate, UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeCell", for: indexPath) as! CountryCodeCell
        let item = arrSearchData[indexPath.row]
        cell.lblFlag.text = item.countryFlag
        cell.lblCountryCode.text = item.countryCode
        cell.lblCountryName.text = item.countryName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let value = arrSearchData[indexPath.row]
        removeWithAnimationWithValue(value: value)
    }
}
