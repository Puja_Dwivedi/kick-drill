//
//  ExamQstnHeaderView.swift
//  MCQ
//
//  Created by cis on 25/01/22.
//

import UIKit

class HintHeaderView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewExamFormulaHint: UIView!
    @IBOutlet weak var viewExamImageHint: UIView!
    @IBOutlet weak var btnExamImageHint: UIButton!
    @IBOutlet weak var btnExamFormula: UIButton!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var imgClock: UIImageView!
    @IBOutlet weak var lblQstnNo: UILabel!
    @IBOutlet weak var ViewQstnNos: UIView!
    @IBOutlet weak var viewExamHeader: UIView!
    @IBOutlet weak var stackViewPracticeHeader: UIStackView!
    @IBOutlet weak var viewFormulaHint: UIView!
    @IBOutlet weak var viewImageHint: UIView!
    @IBOutlet weak var viewHint: UIView!
    @IBOutlet weak var viewQuestionHint: UIView!
    @IBOutlet weak var btnImageHint: UIButton!
    @IBOutlet weak var btnHint: UIButton!
    @IBOutlet weak var btnQuestionHint: UIButton!
    @IBOutlet weak var btnFormula: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
//        setColor()
//        setFont()
        setUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
//        setColor()
//        setFont()
        setUI()
    }
    
//    func setColor() {
//        lblQstnNo.textColor = Colors.AppColor
//        lblTime.textColor = Colors.AppColor
//    }
//
//    func setFont() {
//        lblQstnNo.font = Fonts.SFProTextRegular16
//        lblQstnNo.adjustsFontSizeToFitWidth = true
//        lblQstnNo.minimumScaleFactor = 0.5
//        lblTime.font = Fonts.SFProTextRegular16
//    }
//
//    func setTimerColor() {
//        imgClock.tintColor = Colors.AppColor
//        lblTime.textColor =  Colors.AppColor
//    }
//
//    func setTimerColorChange() {
//        imgClock.tintColor = Colors.PieChartRedColor
//        lblTime.textColor =  Colors.PieChartRedColor
//    }
    
    func setExamHeader() {
        viewExamHeader.isHidden = false
        stackViewPracticeHeader.isHidden = true
        contentView.isHidden = false
    }
    
    func setPracticeHeader() {
        viewExamHeader.isHidden = true
        stackViewPracticeHeader.isHidden = false
        contentView.isHidden = false
    }
    
    func setUI() {
        contentView.isHidden = true
        imgClock.tintAdjustmentMode = .normal
        setFormulaUI()
    }
    
    func setFormulaUI() {
//        btnExamFormula.setTitle("X²", for: .normal)
//        btnExamFormula.titleLabel?.font = Fonts.SFProTextSemibold18
//        btnFormula.setTitle("X²", for: .normal)
//        btnFormula.titleLabel?.font = Fonts.SFProTextSemibold18
    }
    
    func initSubviews() {
        let nib = UINib(nibName: "HintHeaderView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
}
